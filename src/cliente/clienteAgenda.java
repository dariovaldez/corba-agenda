package cliente;

import AgendaApp.*;

import java.util.Properties;
import java.util.Scanner;
import org.omg.CosNaming.NamingContextExt;
import org.omg.CosNaming.NamingContextExtHelper;

public class clienteAgenda {
	
	private static Agenda agendaImpl;

	public static void main(String[] args) {
		try {
			Properties props = System.getProperties();
			props.setProperty("org.omg.CORBA.ORBInitialHost", "localhost");
			props.setProperty("org.omg.CORBA.ORBInitialPort", "1050");
			
			org.omg.CORBA.ORB orb = org.omg.CORBA.ORB.init(args, props);
			org.omg.CORBA.Object objRef = orb.resolve_initial_references("NameService");
			NamingContextExt ncRef = NamingContextExtHelper.narrow(objRef);

			String name = "agenda";
			agendaImpl = AgendaHelper.narrow(ncRef.resolve_str(name));
			
			Scanner reader = new Scanner(System.in);
			System.out.println("Ingrese su nombre: ");
			String nombre = reader.nextLine();
			System.out.println("Ingrese su numero: ");
			String numero = reader.nextLine();
			
			String resultado = agendaImpl.registrar(nombre, numero);
			System.out.println(resultado);

		} catch (Exception e) {
			// TODO: handle exception
		}
	}
	
}
