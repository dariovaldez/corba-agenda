package Server;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;

import AgendaApp.AgendaPOA;
import database.ConectarMariaDB;


public class AgendaImpl extends AgendaPOA{
	
	
	@Override
	public void shutdown() {
		// TODO Auto-generated method stub
	}



	@Override
	public String registrar(String nombre, String numero) {
		String resultado = "comunico con exito";
		try {
			String sql = "insert into agenda(nombre, numero)values(?,?)";
			ConectarMariaDB c = new ConectarMariaDB();
			Connection conn = c.conectarMariaDB();
			PreparedStatement ps = conn.prepareStatement(sql);
			ps.setString(1, nombre);
			ps.setString(2, numero);
			ps.execute();
			resultado = "Registrado con exito";
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return resultado;
	}
	

}
