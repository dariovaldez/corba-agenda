package Server;

import java.util.Properties;

import org.omg.CosNaming.NameComponent;
import org.omg.CosNaming.NamingContextExt;
import org.omg.CosNaming.NamingContextExtHelper;
import org.omg.PortableServer.POA;
import org.omg.PortableServer.POAHelper;
import AgendaApp.*;

public class ServerAgenda {

	public static void main(String[] args) {
		
		try {
			
			Properties props = System.getProperties();	
			props.setProperty("org.omg.CORBA.ORBInitialHost", "localhost");
			props.setProperty("org.omg.CORBA.ORBInitialPort", "1050");
			
			org.omg.CORBA.ORB orb = org.omg.CORBA.ORB.init(args, props);
			POA rootPoa = POAHelper.narrow(orb.resolve_initial_references("RootPOA"));
			rootPoa.the_POAManager().activate();
			AgendaImpl serverImpl = new AgendaImpl();
			
			org.omg.CORBA.Object ref = rootPoa.servant_to_reference(serverImpl);
            Agenda mRef = AgendaHelper.narrow(ref);
 
            org.omg.CORBA.Object objRef = orb.resolve_initial_references("NameService");
            NamingContextExt ncRef = NamingContextExtHelper.narrow(objRef);

			String name = "agenda";
			NameComponent path[] = ncRef.to_name(name);
			ncRef.rebind(path, mRef);

			System.out.println("Servidor iniciado... ");
			orb.run();
			
			
			
		} catch (Exception e) {
			// TODO: handle exception
		}
		
	}
}
