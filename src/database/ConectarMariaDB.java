package database;

import java.sql.*;

public class ConectarMariaDB {
	
	Connection conectar = null;
	String database = "agendadistribuidos";
	String puertoMariaDB= "3310";
	String url = "jdbc:mysql://localhost:"+ puertoMariaDB +"/" +database;
	String user = "root";
	String password = "1234";
	
	public Connection conectarMariaDB() {
		try {
			Class.forName("com.mysql.jdbc.Driver");
			conectar = DriverManager.getConnection(url, user, password);
			System.out.println("Se conect� correctamente al servidor de MariaDB");
		} catch (ClassNotFoundException | SQLException ex) {
			// TODO: handle exception
			System.out.println("No se conect� ");
		}
		return conectar;
	}
	
	public void desconectar() {
		try {
			conectar.close();
			System.out.println("Se desconect� de MariaDB");
		} catch (Exception e) {
			// TODO: handle exception
		}
	}
	
}
